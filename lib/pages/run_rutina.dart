import 'dart:async';

import 'package:cratus/shared_prefs/user_prefs.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class RunRutina extends StatefulWidget {
  RunRutina({Key key}) : super(key: key);

  _RunRutinaState createState() => _RunRutinaState();
}

class _RunRutinaState extends State<RunRutina> {
    static const cratusChannel = const MethodChannel('cratus');

    int milisegundos = 0;
    int minutos = 0;
    final prefs = PreferenciaUsuario();
    int segundos = 0;
    StreamSubscription subscription;
    Timer timer;

    static const _stream = const EventChannel('estados-cratus');

    bool _activo = false;
    bool _mensajes = true;
    String _msj;
    String _newMensaje = '';
    String _oldMensajes = 'old';

  @override
  void dispose() {
    subscription.cancel();
    timer.cancel();
    super.dispose();
  }

    @override
  void initState() {
    super.initState();
    _run();
/*     subscription = _stream.receiveBroadcastStream().listen((mensaje){
      setState(() {
        _msj = mensaje;
        print('MENSAJE STREAM $_msj');
      });
    }); */
  }

    Future<void> _string(String string) async{
      try {
        await cratusChannel.invokeMethod("string", {
          'string' : string
        });
      } on PlatformException catch (e) {
        print(e);
      }
    }
      void _run(){
      int flag = 0;
      int pos = 0;
      String _nuevo;
      String msj;
      String _disp;
      String _color;
      String _nuevoColor;
      List<String> _lista;
      timer = Timer.periodic(
        Duration(milliseconds: 10),
         (timer){
              _mensaje();
              if(_mensajes == true && _newMensaje != _oldMensajes && _activo == true){
                msj = prefs.pasos[flag];
                _lista = msj.split(';');
                _nuevo = _lista[pos];
                _disp = _nuevo[0];
                _color = _nuevo.substring(1);
                if(_lista[pos] == null){
                  flag++;
                  if(prefs.pasos[flag] == null){
                    _activo = false;
                    timer.cancel();
                  }
                }else{
                  if(_color == 'red'){
                    _nuevoColor = 'FF0000';
                  }
                  if(_color == 'green'){
                    _nuevoColor = '00FF00';
                  }
                  if(_color == 'yellow'){
                    _nuevoColor = 'FFFF00';
                  }
                  if(_color == 'purple'){
                    _nuevoColor = 'FF00FF';
                  }
                _string('${prefs.familia};0$_disp;03;00000000${_nuevoColor}020102');
                pos++;
                }
              }
         }
      );
  }

    _mensaje() async{
        try {
          String result = await cratusChannel.invokeMethod("msj");
          if(result != null){
            List<String> disp = result.split(";");
            if(disp[disp.length-1].length > 1 && _oldMensajes != _newMensaje){
                _mensajes = true;
                _oldMensajes = _newMensaje;
                _newMensaje = disp[disp.length-1];
            }else{
                _mensajes = false;
            }
            
            // if(disp[disp.length-1].length == 1){
            //   _mensajes = false;
            // }else{
            //     _mensajes = true;
            //     _oldMensajes = _newMensaje;
            //     _newMensaje = result;
            // }
          }
        } on PlatformException catch (e) {
          print(e);
        }
      
    }

    Widget contenedorTiempo(double altura) {
    return Container(
      margin: EdgeInsets.all(30),
      height: altura * 0.18,
      decoration: BoxDecoration(
        color: Color.fromRGBO(224, 226, 228, 1),
        borderRadius: BorderRadius.circular(20),
      ),
      child: Center(
        // Text(_mostrar, style: TextStyle(fontSize: 40),),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
              Container(
              padding: EdgeInsets.all(20),
              width: 50,
              child: Text(minutos.toString(), style: TextStyle(fontSize: 40),textAlign: TextAlign.justify)
              ),
              Text(':', style: TextStyle(fontSize: 40)),
              Container(
              width: 50,
              child: Text(segundos.toString(), style: TextStyle(fontSize: 40),textAlign: TextAlign.justify)
              ),
               Text(':', style: TextStyle(fontSize: 40)),
            Center(          
              child: Container(
                width: 50,
                child: Text(milisegundos.toString(), style: TextStyle(fontSize: 40),textAlign: TextAlign.justify,)
                ),
            )
          ],
        )
      ),
    );
  }

  Widget _boton(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        SizedBox(
          height: 90,
          width: 90,
          child: IconButton(
            padding: EdgeInsets.all(0),
            icon: Icon(Icons.play_arrow, size: 90, color: Colors.pink,),
            onPressed: (){
              if(_activo == false)
                _play();
                },
            ),
        ),
          SizedBox(
              height: 90,
              width: 90,
              child: IconButton(
              padding: EdgeInsets.all(0),
              alignment: Alignment.center,
              icon: Icon(Icons.pause, size:90, color: Colors.pink,),
              onPressed: (){
                _activo = false;
                setState(() { });
              },
            ),
          ),
          SizedBox(
              height: 90,
              width: 90,
              child: IconButton(
              padding: EdgeInsets.all(0),
              alignment: Alignment.center,
              icon: Icon(Icons.stop, size:90, color: Colors.pink,),
              onPressed: (){
                milisegundos = 0;
                segundos = 0;
                minutos = 0;
                _activo = false;
                setState(() {});
              },
            ),
          ),
      ],
    );
    }

    void _play() {
      setState(() {
        _activo = true;
      });

      timer = Timer.periodic(
        Duration(milliseconds: 10),
         (Timer timer){
           if(_activo == true){
             milisegundos += 1;
           if(milisegundos > 99){
             milisegundos = 0;
             segundos = segundos + 1;
           }
           if(segundos > 59){
             segundos = 0;
             minutos = minutos + 1;
           }
              setState(() {});
           }
         }
      );
    }

  @override
  Widget build(BuildContext context) {
    double _altura = MediaQuery.of(context).size.height;
    // double _ancho  = MediaQuery.of(context).size.width;

    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(_altura * 0.15),
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(bottomLeft: Radius.circular(30),bottomRight: Radius.circular(30)),
            image: DecorationImage(
              image: AssetImage('assets/img/barra_run.png'),
              fit: BoxFit.cover
            ),
          ),
          child: AppBar(
            elevation: 10,
            title: Text('REACCIÓN', style: TextStyle(fontSize: 25),),
            backgroundColor: Colors.transparent,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.vertical(
              bottom: Radius.circular(30),
              ),
            ),
          ),
        ),
      ),
      body: Column(
        children: <Widget>[
          SizedBox(height: _altura * 0.05,),
          contenedorTiempo(_altura),
          _boton(context),
          SizedBox(height: _altura * 0.05,)
        ],

      ),
    );
  }
}