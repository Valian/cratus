import 'dart:async';

import 'package:cratus/shared_prefs/user_prefs.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
 
class Conectar extends StatefulWidget {
  @override
  _ConectarState createState() => _ConectarState();
}
class _ConectarState extends State<Conectar> {
  static const cratusChannel = const MethodChannel('cratus');

  final prefs = PreferenciaUsuario();

  // List _dispositivos;

  @override
  initState() {
    super.initState();
    _getDispositivos();
  }

    Future<List> _getDispositivos() async{
    List dispositivos = new List();
    dispositivos = null;
    try {
      final List result = await cratusChannel.invokeMethod("buscar");
      dispositivos = result;
    } on PlatformException catch (e) {
      print(e);
    }
      return dispositivos.toSet().toList();
    }
    Future<void> _mensaje() async{
      try {
        String result = await cratusChannel.invokeMethod("familia");
        prefs.familia = result;
      } on PlatformException catch (e) {
        print(e);
      }
    }

   _conectar(String mac) async{
      try {
        await cratusChannel.invokeMethod("conectar", {
          'mac' : mac
        });
        await _mensaje();
      } on PlatformException catch (e) {
        print(e);
      }
    }
    Future<void> _desc() async{
      try {
        await cratusChannel.invokeMethod("desc");
      } on PlatformException catch (e) {
        print(e);
      }
    }


  Widget _builder(List dispositivos) {
    List<String> mac;
    String _mac;
    String conectar = 'Conectar';
    String desconectar = 'Desconectar';
    String texto;

      Widget _row(String mac, String nombre){
        String _mac = mac;
        Color _color;
        if(prefs.mac == _mac){
          texto = desconectar;
          _color = Colors.red;
          _conectar(mac);
        }
        if(prefs.mac != _mac){
          texto = conectar;
          _color = Colors.blue;
        }
        return Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Text(nombre, textAlign: TextAlign.left,),
            SizedBox(width: 30,),
            Expanded(
              child: Container(
              ),
            ),
            RaisedButton(
              color: _color,
              onPressed: ()async{
                if(prefs.mac != _mac){
                  prefs.mac = _mac;
                   _mensaje();
                  // print("---------------FAMILIA ${prefs.familia}--------");
                }else {
                  _desc();
                  prefs.mac = '';
                }
                setState(() {});
              },
              child: Text(texto),
            )
          ],
        );
      }
    return ListView.builder(
        padding: EdgeInsets.all(30),
        scrollDirection: Axis.vertical,
        itemCount: dispositivos.length,
        itemBuilder: (BuildContext context, int index){
          List<String> _nombre = dispositivos[index].split(' ');
          mac = dispositivos[index].split('== ');
          _mac = mac[1];
          return _row(_mac, _nombre[0]);
        },
      );  
    }
      @override
      Widget build(BuildContext context) {
    
      double _altura = MediaQuery.of(context).size.height;
        return Scaffold(
            appBar: PreferredSize(
            preferredSize: Size.fromHeight(_altura * 0.15),
            child: Container(
              child: AppBar(
                leading: Icon(Icons.arrow_left, color: Colors.black),
                elevation: 10,
                title: Text('VINCULACIÓN', style: TextStyle(fontSize: 25, color: Colors.red),),
                backgroundColor: Colors.white,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.vertical(
                  bottom: Radius.circular(30),
                  ),
                ),
              ),
            ),
          ),
            body: RefreshIndicator(
                  onRefresh: ()async{
                    setState(() {
                      _getDispositivos();
                    });
                  },
                  child: Stack(
                    children: <Widget>[
                      FutureBuilder(
                      future: _getDispositivos(),
                      builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
                        if(snapshot.hasData) return _builder(snapshot.data);
                          return Center(
                            child: CircularProgressIndicator(),
                          );
                      }
                      ),
                      Column(
                        children: <Widget>[
                          Expanded(
                            child: Container(),
                          ),
                          RaisedButton(
                            child: Text('BUSCAR'),
                            onPressed: (){
                              setState(() {
                                _getDispositivos();
                              });
                            },
                          )
                        ],
                      )
                    ],
                  ),
                )
          );
      }
}