import 'package:flutter/material.dart';

class ResultadoPage extends StatelessWidget {
  const ResultadoPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double _altura = MediaQuery.of(context).size.height;
    // double _ancho = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(_altura * 0.15),
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(bottomLeft: Radius.circular(30),bottomRight: Radius.circular(30)),
            image: DecorationImage(
              image: AssetImage('assets/img/barra_superior_aleatorio.png'),
              fit: BoxFit.cover
            ),
          ),
          child: AppBar(
            elevation: 10,
            title: Text('RESULTADO', style: TextStyle(fontSize: 25),),
            backgroundColor: Colors.transparent,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.vertical(
              bottom: Radius.circular(30),
              ),
            ),
          ),
        ),
      ),
      body: Text('Resultado'),
    );
  }
}