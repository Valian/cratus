import 'package:cratus/shared_prefs/user_prefs.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';

import 'correr_rutina.dart';

class AleatoriaPage extends StatefulWidget {
  @override
  _AleatoriaPageState createState() => _AleatoriaPageState();
}
Duration oneDecimal =  Duration(milliseconds: 15000);
class _AleatoriaPageState extends State<AleatoriaPage> {
  String iconoColor = 'assets/img/botones_de_colores.png';
  final prefs = PreferenciaUsuario();
  String tiempo = '00 : 00 : 00';
  static const cratusChannel = const MethodChannel('cratus');
  DateTime _temporizador = new DateTime(0);
  
  Widget _tiempo(double altura, double ancho) {
    return Container(
      margin: EdgeInsets.all(20),
      height: altura * 0.25,
      decoration: BoxDecoration(
        color: Color.fromRGBO(224, 226, 228, 1),
        borderRadius: BorderRadius.circular(20)
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(height: 5,),
          Container(
            margin: EdgeInsets.only(left: ancho *0.05),
            child: Form(
              child: Text('Indica el tiempo de ejercicio', style: TextStyle(fontSize: 18),)
              )
            ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              GestureDetector(
                onTap:(){
                  DatePicker.showTimePicker(
                    context,
                    theme: DatePickerTheme(
                      cancelStyle: TextStyle(fontSize: 20, color: Colors.pink),
                      doneStyle: TextStyle(fontSize: 20, color: Colors.pink),
                      itemStyle: TextStyle(fontSize: 20, color: Colors.pink),
                      backgroundColor: Colors.white,
                      containerHeight: altura* 0.3,
                    ),
                    showTitleActions: true,
                    onConfirm: (time){
                      tiempo = '${time.hour}h : ${time.minute}m : ${time.second}s';
                      setState(() {
                        _temporizador = time;
                      });
                    },
                    currentTime: DateTime(0),
                    locale: LocaleType.es
                    );
                },
                child: Container(
                  margin: EdgeInsets.only(top: 20),
                  height: altura * 0.15,
                  width: ancho * 0.6,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(15)
                  ),
                  child: Center(
                    child:Text(tiempo, style: TextStyle(fontSize: 35),),
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
    @override
    void initState() { 
      super.initState();
      _conectar(prefs.mac);
    }
    Widget _seleccionarColores(double altura, double ancho) {
    return Container(
      margin: EdgeInsets.all(20),
      height: altura * 0.25,
      decoration: BoxDecoration(
        color: Color.fromRGBO(224, 226, 228, 1),
        borderRadius: BorderRadius.circular(20)
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(height: 5,),
          Container(
            margin: EdgeInsets.only(left: ancho *0.05),
            child: Column(
              children: <Widget>[
                Text('Colores para trabajar', style: TextStyle(fontSize: 18),),
              ],
            )
            ),
          Container(
            margin: EdgeInsets.only(left: ancho * 0.05, right: ancho *0.05, top: ancho *0.05),
            height: altura * 0.15,
            width: ancho * 0.9,
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(15)
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                _crearBoton(iconoColor, Colors.green, altura, ancho),
                SizedBox(width: altura * 0.02,),
                _crearBoton(iconoColor, Colors.yellow, altura, ancho),
                SizedBox(width: altura * 0.02,),
                _crearBoton(iconoColor, Colors.red, altura, ancho),
                SizedBox(width: altura * 0.02,),
                _crearBoton(iconoColor, Colors.purple, altura, ancho),
              ],
            ),
          ),
        ],
      ),
    );
  }


  Future<void> _conectar(String mac) async{
      try {
        await cratusChannel.invokeMethod("conectar");
      } on PlatformException catch (e) {
        print(e);
      }
    }
  Widget _ubicarModulos(double altura, double ancho) {
    return Container(
      margin: EdgeInsets.all(ancho * 0.05),
      height: altura * 0.5,
      decoration: BoxDecoration(
        color: Color.fromRGBO(224, 226, 228, 1),
        borderRadius: BorderRadius.circular(20)
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(height: 5,),
          Container(
            margin: EdgeInsets.only(left: ancho *0.05),
            child: Column(
              children: <Widget>[
                Text('Ubica tus modulos', style: TextStyle(fontSize: 18),),
              ],
            )
            ),
          Container(
              margin: EdgeInsets.all(ancho * 0.05),
              height: altura * 0.38,
              width: ancho * 0.9,
              decoration: BoxDecoration(
                color: Color.fromRGBO(242, 93, 105, 1),
                borderRadius: BorderRadius.circular(30)
            ),
            child: Container(
              child: Column(
                children: <Widget>[
                  SizedBox(height: altura *0.05),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      _crearBotonModulo(iconoColor, Colors.transparent),
                      _crearBotonModulo(iconoColor, Colors.transparent),
                    ],
                  ),
                  SizedBox(height: altura *0.11),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      _crearBotonModulo(iconoColor, Colors.transparent),
                      _crearBotonModulo(iconoColor, Colors.transparent),
                    ],
                  ),
                ],
              )
            ),
          ),
        ],
      ),
    );
  }

    Widget _crearBotonModulo(String icono, Color color) {
    return Container(
      // margin: EdgeInsets.only(top: 20, bottom: 100),
      child: FloatingActionButton(
          backgroundColor: color,
          onPressed: null,
          elevation: 10,
          heroTag: null,
          child: Image(
            image: AssetImage(icono),
            fit: BoxFit.cover,
          ),
      ),
    );
  }

  Widget _boton(BuildContext context) {
    return Column(
         children: <Widget>[
           Expanded(
             child: Container(),
           ),
             Builder(
              builder:(context)=> FloatingActionButton(
                 heroTag: null,
                  elevation: 10,
                  backgroundColor: Colors.black,
                  child: Icon(Icons.play_arrow, size: 50),
                  onPressed: (){
                    if(prefs.aleatorioGreen == false && prefs.aleatoriopurple == false && prefs.aleatorioRed == false && prefs.aleatorioYellow == false){
                      final snackBar =  SnackBar(
                        content: Text('Debe seleccionar al menos un color'),
                        action: SnackBarAction(
                          label: 'Cerrar',
                          onPressed: (){},
                        ),
                      );
                      Scaffold.of(context).showSnackBar(snackBar);
                    }
                    if(_temporizador.toString() != '0000-01-01 00:00:00.000'){
                      Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context)=> CorrerRutina(tiempo :_temporizador),
                      )
                    );
                    }
                      final snackBar =  SnackBar(
                        content: Text('El tiempo debe ser distinto de 0'),
                        action: SnackBarAction(
                          label: 'Cerrar',
                          onPressed: (){},
                        ),
                      );
                      Scaffold.of(context).showSnackBar(snackBar);
                  },
            ),
             ),
          SizedBox(height: 20,)
         ], 
       );
  }

    Widget _crearBoton(String icono, Color color, double altura, double ancho) {
      Color fondo;
      if(color == Colors.red){
        if(prefs.aleatorioRed == true)
            fondo = color;
        else{
          fondo = color.withOpacity(0.4);
        }
      }
      if(color == Colors.yellow){
        if(prefs.aleatorioYellow == true)
            fondo = color;
        else{
          fondo = color.withOpacity(0.4);
        }
      }
      if(color == Colors.green){
        if(prefs.aleatorioGreen == true)
            fondo = color;
        else{
          fondo = color.withOpacity(0.4);
        }
      }
      if(color == Colors.purple){
        if(prefs.aleatoriopurple == true)
            fondo = color;
        else{
          fondo = color.withOpacity(0.4);
        }
      }
        _desactivar(){
          if(color == Colors.red){
            if(prefs.aleatorioRed== true)
              prefs.aleatorioRed = false;
            else{
              prefs.aleatorioRed = true;
            }
          }
          if(color == Colors.green){
            if(prefs.aleatorioGreen== true)
              prefs.aleatorioGreen = false;
            else{
              prefs.aleatorioGreen = true;
            }
          }
          if(color == Colors.purple){
            if(prefs.aleatoriopurple== true)
              prefs.aleatoriopurple = false;
            else{
              prefs.aleatoriopurple = true;
            }
          }
          if(color == Colors.yellow){
            if(prefs.aleatorioYellow== true)
              prefs.aleatorioYellow = false;
            else{
              prefs.aleatorioYellow = true;
            }
          }
        }
    return Container(
      width: ancho *0.15,
      child: FloatingActionButton(
          backgroundColor: fondo,
          onPressed: (){
            setState(() {
             _desactivar(); 
            });
          },
          elevation: 10,
          heroTag: null,
          child: Image(
            image: AssetImage(icono),
            fit: BoxFit.cover,
          ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    double _altura = MediaQuery.of(context).size.height;
    double _ancho = MediaQuery.of(context).size.width;
    return Scaffold(
        appBar: PreferredSize(
        preferredSize: Size.fromHeight(_altura * 0.15),
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(bottomLeft: Radius.circular(30),bottomRight: Radius.circular(30)),
            image: DecorationImage(
              image: AssetImage('assets/img/barra_superior_aleatorio.png'),
              fit: BoxFit.cover
            ),
          ),
          child: AppBar(
            elevation: 10,
            title: Text('ALEATORIO', style: TextStyle(fontSize: 25),),
            backgroundColor: Colors.transparent,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.vertical(
              bottom: Radius.circular(30),
              ),
            ),
          ),
        ),
      ),
      body: Stack(
        children: <Widget>[
            SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Container(
            child: Column(
              children: <Widget>[
                _tiempo(_altura, _ancho),
                _seleccionarColores(_altura, _ancho),
/*                 _ubicarModulos(_altura, _ancho), */
                _escoger(),
                SizedBox(height: _altura * 0.1,)
              ],
            )
          ),
        ),
        _boton(context),
        SizedBox(height: 10,)
        ],
      ),
    );
  }

  Widget _escoger() {
    int _dispo;
    return Container(
/*       margin: EdgeInsets.all(20),
      child: Row(
        children: <Widget>[
          ListTile(
            title: const Text('Lafayette'),
            leading: Radio(
              value: 1,
              groupValue: _dispo,
              onChanged: (value) {
                setState(() { _dispo = value; });
              },
            ),
          ),
        ],
      ), */
    );
  }
}