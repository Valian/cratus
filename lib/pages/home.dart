import 'package:cratus/pages/conectar.dart';
import 'package:cratus/pages/inicio_page.dart';
import 'package:cratus/pages/menu.dart';
import 'package:flutter/material.dart';


class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  int currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _callPage(currentIndex),
      bottomNavigationBar: _crearBottomNavigationBar(),
    );
  }

  Widget _crearBottomNavigationBar() {
    return BottomNavigationBar(
      backgroundColor: Color.fromRGBO(27, 38, 49, 1),
      currentIndex: currentIndex,
      onTap: (index){
        setState(() {
         currentIndex = index; 
        });
      },
      items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home, size: 60),
            title: Container()
          ),
          BottomNavigationBarItem(
              icon: Image.asset('assets/img/modos.png'),
              title: Container()
          ),
          BottomNavigationBarItem(
            icon: Image.asset('assets/img/vinculacion.png',),
            title: Container(),
          ),
      ],
    );
  }

  Widget _callPage(int paginaActual) {
    switch( paginaActual ){
      case 0 : return InicioPage();
      case 1 : return MenuPage();
      case 2 : return Conectar();

      default: return InicioPage();
    }
  }
}