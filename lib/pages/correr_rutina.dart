import 'dart:async';
import 'dart:math';

import 'package:cratus/shared_prefs/user_prefs.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class CorrerRutina extends StatefulWidget {
  CorrerRutina({
    Key key,
    @required this.tiempo,
  }): mostrarTiempo = '${tiempo.hour}h : ${tiempo.minute}m : ${tiempo.second}s';

  @override
  _CorrerRutinaState createState() => _CorrerRutinaState();

  String mostrarTiempo;
  final DateTime tiempo;
}

class _CorrerRutinaState extends State<CorrerRutina> {
  static const cratusChannel = const MethodChannel('cratus');

  bool activo = false;
  bool enviar = false;
  final prefs = PreferenciaUsuario();
  Timer tiempo;
  Timer timer;

  List<String> _lista = new List();
  String _mensajes = 'nuevo';
  // String _mensajes = '';
  // String _nuevo = '';
  String _oldMensaje = 'old';
  // String _oldMensajes = 'old';
  // String _oldNuevo = 'old';
  @override
  void dispose() {
    timer.cancel();
    tiempo.cancel();
    super.dispose();
  }
  @override
  void initState() {
    super.initState();
    _conectar(prefs.mac);
    _inicializar();
    
         _run();
    
        if(prefs.aleatorioGreen == true)
          _lista.add('00FF00');
        if(prefs.aleatoriopurple == true)
          _lista.add('FF00FF');
        if(prefs.aleatorioRed == true)
          _lista.add('FF0000');
        if(prefs.aleatorioYellow == true)
          _lista.add('FFFF00');
      }
    
      // @override
      // void dispose() {
    
      //   super.dispose();
      //   tiempo.cancel();
      //   _timer.cancel();
      // }
    
        Future<void> _string(String string) async{
          try {
            await cratusChannel.invokeMethod("string", {
              'string' : string
            });
          } on PlatformException catch (e) {
            print(e);
          }
        }
    
         Future<void> _conectar(String mac) async{
          try {
            await cratusChannel.invokeMethod("conectar");
          } on PlatformException catch (e) {
            print(e);
          }
        }
    
        _mensaje() async{
          var random = Random.secure();
          var value = random.nextInt(_lista.length);
          var numero = random.nextInt(4);
            numero +=1;
          var dolar = Runes('\u{0024}');
          String dolarString = new String.fromCharCodes(dolar);
            
            String result = await cratusChannel.invokeMethod("msj");/* 
            print('MENSAJE $result'); */
            if(result != null && _oldMensaje != result){
              List<String> disp = result.split(";");
              _mensajes = result;
                  if(disp[disp.length-1].length > 1 && activo == true){      
                    if(_mensajes != null){
                      await _string("${prefs.familia};0$numero;03;00000000${_lista[value]}020102");
                      _oldMensaje = _mensajes;
                    }
                  }
            }
        }
    
      Widget contenedorTiempo(double altura, String _mostrar) {
        return Container(
          margin: EdgeInsets.all(30),
          height: altura * 0.18,
          decoration: BoxDecoration(
            color: Color.fromRGBO(224, 226, 228, 1),
            borderRadius: BorderRadius.circular(20),
          ),
          child: Center(
            child:Text(_mostrar, style: TextStyle(fontSize: 40),),
          ),
        );
      }
    
      Widget _tiempoEsteblecido(double altura, double ancho) {
        return Container(
          height: altura * 0.05,
          width: ancho * 0.4,
          decoration: BoxDecoration(
            color: Color.fromRGBO(224, 226, 228, 1),
            borderRadius: BorderRadius.circular(10)
          ),
          child: Container(
            margin: EdgeInsets.only(left: ancho *0.08, top:altura*0.009),
            child: Text('${widget.tiempo.hour}h : ${widget.tiempo.minute}m : ${widget.tiempo.second}s', style: TextStyle(fontSize: 20),)
          ),
        );
      }
    
        Widget _boton(BuildContext context, int stop) {
        return Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            SizedBox(
              height: 90,
              width: 90,
              child: IconButton(
                padding: EdgeInsets.all(0),
                icon: Icon(Icons.play_arrow, size: 90, color: Colors.pink,),
                onPressed: (){
                  if(activo == false){
                    _play(stop);
                    }
                  activo = true;
                  
                    },
                ),
            ),
              SizedBox(
                  height: 90,
                  width: 90,
                  child: IconButton(
                  padding: EdgeInsets.all(0),
                  alignment: Alignment.center,
                  icon: Icon(Icons.stop, size:90, color: Colors.pink,),
                  onPressed: (){
                    activo = false;
                    setState(() {});
                  },
                ),
              )
          ],
        );
        }
    
        void _play(int stop) {
          setState(() {
            activo = true;
            enviar = true;
          });
          int _stop = stop;
          tiempo = Timer.periodic(
            Duration(seconds: 1),
             (Timer timer){
               if(_stop<1 || activo == false ){
                 timer.cancel();
               }else{
                 _stop= _stop -1;
                 String nuevoTiempo = _setTiempo(_stop);
                  widget.mostrarTiempo = nuevoTiempo;
                  setState(() {});
               }
             }
          );
        }
    
      String _setTiempo(int stop) {
    
        double horas=0;
        double minutos=0;
        double segundos = 0;
    
        if(stop >= 3600 ){
          horas = stop /3600;
          minutos = stop / 60;
          while(minutos >= 60){
            minutos = minutos - 60;
          }
          segundos = minutos % 1 * 60;
        }
        if(stop > 60 && stop < 3600){
          minutos = stop / 60;
          segundos = minutos % 1 * 60;
        }
        if(stop <= 60){
          segundos = stop.toDouble();
        }
        return '${horas.round()}h : ${minutos.floor()}m : ${segundos.round()}s';
      }
    
      void _run() {
          timer = Timer.periodic(
          Duration(milliseconds: 1000),
            (timer){
              if(activo == true){
                if(_mensajes != _oldMensaje){
                  _mensaje();
                }
              }
              //  if(enviar == true && activo == true){
              //    var random = Random.secure();
              //    var value = random.nextInt(_lista.length);
              //    var numero = random.nextInt(4);
              //    _string("0002;01;03;00000000FF0000020102");
              //    enviar = false;
              //  }
            }
        );
      }
    
      @override
      Widget build(BuildContext context) {
    
        final _stop = widget.tiempo.hour * 3600 + widget.tiempo.minute * 60 + widget.tiempo.second;
        String _mostrar = widget.mostrarTiempo;
    
        double _altura = MediaQuery.of(context).size.height;
        double _ancho = MediaQuery.of(context).size.width;
        return Scaffold(
          appBar: PreferredSize(
            preferredSize: Size.fromHeight(_altura * 0.15),
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(bottomLeft: Radius.circular(30),bottomRight: Radius.circular(30)),
                image: DecorationImage(
                  image: AssetImage('assets/img/barra_superior_aleatorio.png'),
                  fit: BoxFit.cover
                ),
              ),
              child: AppBar(
                elevation: 10,
                title: Text('RUTINA', style: TextStyle(fontSize: 25),),
                backgroundColor: Colors.transparent,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.vertical(
                  bottom: Radius.circular(30),
                  ),
                ),
              ),
            ),
          ),
          body: Column(
            children: <Widget>[
              SizedBox(height: _altura * 0.05,),
              contenedorTiempo(_altura, _mostrar),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Text('Tiempo establecido:'),
                  _tiempoEsteblecido(_altura, _ancho),
                ],
              ),
              SizedBox(height: _altura *0.1,),
              _boton(context, _stop),
              SizedBox(height: _altura * 0.05,),

            ],
          ),
        );
      }
    
      void _inicializar() async{
        await  _string("${prefs.familia};01;04;00000000000000000000");
        await  _string("${prefs.familia};02;04;00000000000000000000");
        await  _string("${prefs.familia};03;04;00000000000000000000");
        await  _string("${prefs.familia};04;04;00000000000000000000");
        await  _string("${prefs.familia};05;04;00000000000000000000");
        await  _string("${prefs.familia};06;04;00000000000000000000");
      }
}