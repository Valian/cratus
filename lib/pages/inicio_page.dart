import 'package:flutter/material.dart';

class InicioPage extends StatelessWidget {
  //stack de la pagina de inicio
  Widget _stack(double _altura, double _ancho) {
    return Stack(
      children: <Widget>[
        _imagenFondo(),
        _titulo(_altura),
        Column(
          children: <Widget>[
        Expanded(
          child: Container(),
        ),
        _cratus(_ancho),
          SizedBox(height: _altura * 0.03,)
          ],
        )
      ],
    );
  }

    Widget _imagenFondo() {
    return Container(
      width: double.infinity,
      height: double.infinity,
      child: Image(
        image: AssetImage('assets/img/fondo.png'),
        fit: BoxFit.cover,
      ),
    );
  }

  Widget _titulo(double alto) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        SizedBox(height: alto * 0.15,),
         Center(
           child: Image(
            image: AssetImage('assets/img/trainlight.png'),
        ),
         ),
      ],
    );
  }

  Widget _cratus(double _ancho) {
    return Center(
      child:Image(
        image: AssetImage('assets/img/cratus.png'),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    double _altura = MediaQuery.of(context).size.height;
    double _ancho = MediaQuery.of(context).size.width;

    return Scaffold(
      body: Container(
        child: _stack(_altura, _ancho)
      ),
      );
      // bottomNavigationBar: new Nav(),
  }
}