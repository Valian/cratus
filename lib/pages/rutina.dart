import 'package:cratus/shared_prefs/user_prefs.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class RutinaPage extends StatefulWidget {
  @override
  _RutinaPage createState() => _RutinaPage();
}

class _RutinaPage extends State<RutinaPage> {
  String iconoColor = 'assets/img/botones_de_colores.png';
  String iconoNumero = 'assets/img/boton_numeros_encendido.png';
  String paso = '';
  final prefs = PreferenciaUsuario();
  List<String> rutina;
  ScrollController _scrollController = new ScrollController();
  static const cratusChannel = const MethodChannel('cratus');
 @override
  void initState() {
    super.initState();
    rutina = prefs.pasos;
    _conectar(prefs.mac);
  }

  Future<void> _conectar(String mac) async{
      try {
        await cratusChannel.invokeMethod("conectar");
      } on PlatformException catch (e) {
        print(e);
      }
    }
    Widget _contentenedor(altura, anchura) {
    return Container(
      margin: EdgeInsets.all(10),
      height: altura * 0.8,
      decoration: BoxDecoration(
        color: Color.fromRGBO(224, 226, 228, 1),
        borderRadius: BorderRadius.circular(30),
      ),
      child: Container(
        margin: EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(30)
        ),
      ),
    );
  }

  Widget _luces(ancho, contador, String fila, double alto) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        _crearBotonPrincipal(iconoNumero, contador, alto, ancho),
        SizedBox(width: ancho *0.01,),
        _crearBoton(iconoColor, Colors.green, ancho, fila, alto),
        SizedBox(width: ancho *0.01,),
        _crearBoton(iconoColor, Colors.yellow, ancho, fila, alto),
        SizedBox(width: ancho *0.01,),
        _crearBoton(iconoColor, Colors.red, ancho, fila, alto),
        SizedBox(width: ancho *0.01,),
        _crearBoton(iconoColor, Colors.purple, ancho, fila, alto),
      ],
    );
  }

  Widget _crearBoton(String icono, Color color , double ancho, String fila, double alto) {
    String add;
    _add(){
      if(fila == 'primero'){
        if(color == Colors.green)
          add ='1green';
        if(color == Colors.yellow)
          add ='1yellow';
        if(color == Colors.red)
          add ='1red';
        if(color == Colors.purple)
          add ='1purple';
      }
      if(fila == 'segundo'){
        if(color == Colors.green)
          add ='2green';
        if(color == Colors.yellow)
          add ='2yellow';
        if(color == Colors.red)
          add ='2red';
        if(color == Colors.purple)
          add ='2purple';
      }
      if(fila == 'tercero'){
        if(color == Colors.green)
          add ='3green';
        if(color == Colors.yellow)
          add ='3yellow';
        if(color == Colors.red)
          add ='3red';
        if(color == Colors.purple)
          add ='3purple';
      }
      if(fila == 'cuarto'){
        if(color == Colors.green)
          add ='4green';
        if(color == Colors.yellow)
          add ='4yellow';
        if(color == Colors.red)
          add ='4red';
        if(color == Colors.purple)
          add ='4purple';
      }
      if(fila == 'quinto'){
        if(color == Colors.green)
          add ='5green';
        if(color == Colors.yellow)
          add ='5yellow';
        if(color == Colors.red)
          add ='5red';
        if(color == Colors.purple)
          add ='5purple';
      }
      if(fila == 'sexto'){
        if(color == Colors.green)
          add ='6green';
        if(color == Colors.yellow)
          add ='6yellow';
        if(color == Colors.red)
          add ='6red';
        if(color == Colors.purple)
          add ='6purple';
      }
      paso = paso + add+';';
    }

    return Container(
      margin: EdgeInsets.all(ancho*0.01),
      width: ancho * 0.125,
      height: alto * 0.125,
      child: FloatingActionButton(
          backgroundColor: color,
          onPressed: (){
            setState(() {
              _add();
              _scrollController.animateTo(
                  _scrollController.position.maxScrollExtent,
                  curve: Curves.easeOut,
                  duration: const Duration(milliseconds: 200),
              );  
            });
          },
          elevation: 10,
          heroTag: null,
          child: Image(
            height: alto * 0.2,
            image: AssetImage(icono),
            fit: BoxFit.cover,
          ),
      ),
    );
  }

  Widget _crearBotonPrincipal(String icono, int contador, double alto, double ancho) {
    return Container(
      margin: EdgeInsets.all(ancho*0.01),
      width: ancho * 0.125,
      height: alto * 0.125,
      child: Stack(
          children: <Widget>[
          Image(
            height: alto,
          image: AssetImage(icono),
          fit: BoxFit.cover,
        ),
        Center(child: Text(contador.toString(), style: TextStyle(fontSize: 40, color: Colors.black),))
          ],
        )
    );
  }

  Widget _boton(BuildContext context) {
    return Center(
       child: Column(
         children: <Widget>[
           Expanded(
             child: Container(),
           ),
           FloatingActionButton(
             heroTag: null,
              elevation: 10,
              backgroundColor: Colors.red,
              child: Icon(Icons.add, size: 40),
              onPressed: ()async{
                if(paso == ''){
                final snackBar =  SnackBar(
                  content: Text('Debe ingresar el nuevo paso'),
                  action: SnackBarAction(
                    label: 'Cerrar',
                    onPressed: (){},
                  ),
                );
                Scaffold.of(context).showSnackBar(snackBar);
                }else{
                  final snackBar =  SnackBar(
                  content: Text('Nuevo paso agregado!'),
                  action: SnackBarAction(
                    label: 'Cerrar',
                    onPressed: (){},
                  ),
                );
                Scaffold.of(context).showSnackBar(snackBar);
                String _final = paso.substring(0, paso.length - 1);
                rutina.add(_final);
                prefs.pasos = rutina;
                paso = '';
                Scaffold.of(context).openDrawer();
                setState(() {});
                }
              },
          ),
          SizedBox(height: 10,)
         ], 
       ),
    );
  }

  Drawer _drawer(double alto, double ancho, BuildContext context) {
    int count = rutina.length;
    return Drawer(
      child: Container(
        decoration: BoxDecoration(
        ),
        child: Column(
          children: <Widget>[
            Container(
              height: alto * 0.2,
              width: ancho,
              decoration: BoxDecoration(
                color: Colors.red,
                borderRadius: BorderRadius.only(bottomLeft: Radius.circular(30),),
              ),
              child: Column(
                children: <Widget>[
                    SizedBox(height: alto * 0.06,),
                  Text('RUTINA', style: TextStyle(fontSize: 25, fontFamily: 'Helvetica',),),
                ],
              )
              ),
            Expanded(
              child: ListView.builder(
                scrollDirection: Axis.vertical,
                itemCount: count,
                itemBuilder: (BuildContext context, int index){
                  return _botonPaso(index + 1);
                },
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                     Builder(
                       builder: (context)=>FloatingActionButton(
                    heroTag: null,
                      elevation: 10,
                      backgroundColor: Colors.black,
                      child: Icon(Icons.play_arrow, size: 50),
                      onPressed: (){
                        if(prefs.pasos.length == 0){
                           final snackBar =  SnackBar(
                          content: Text('Ingresa un paso!'),
                          action: SnackBarAction(
                            label: 'Cerrar',
                            onPressed: (){},
                          ),
                        );
                        Scaffold.of(context).showSnackBar(snackBar);
                        }else{
                          Navigator.of(context).pushNamed('run');
                        }
                      },
                  ),
                ),
                FloatingActionButton(
                  heroTag: null,
                    elevation: 10,
                    backgroundColor: Colors.black,
                    child: Icon(Icons.restore, size: 50),
                    onPressed: (){
                      rutina = [];
                      prefs.pasos = rutina;
                      setState(() {
                        
                      });
                    },
                ),
              ],
              ),
            
          ],
        ),
      ),
    );
  }

  Widget _botonPaso(int paso) {
    return Container(
      margin: EdgeInsets.only(top: 20, left: 20, right: 20),
      width: 250,
      height: 50,
      child: RaisedButton(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
        color: Colors.red[200],
        child: Text('PASO $paso'),
        onPressed: (){},
      ),
    );
  }

  Widget _listaRutina() {
    int modulo;
    Color color;
    List<String> modulos = paso.split(";");
    int  length = modulos.length -1;
    Widget _modulo(index){
      String mod = modulos[index-1][0];
      String colorMod = modulos[index-1].substring(1);
      if(mod == '1'){
        modulo = 1;
      }
      if(mod == '2'){
        modulo = 2;
      }
      if(mod == '3'){
        modulo = 3;
      }
      if(mod == '4'){
        modulo = 4;
      }
      if(mod == '5'){
        modulo = 5;
      }
      if(mod == '6'){
        modulo = 6;
      }
      if(colorMod == 'red'){
        color = Colors.red;
      }
      if(colorMod == 'yellow'){
        color = Colors.yellow;
      }
      if(colorMod == 'green'){
        color = Colors.green;
      }
      if(colorMod == 'purple'){
        color = Colors.purple;
      }
      return Container(
            decoration: BoxDecoration(
              color: color,
              borderRadius: BorderRadius.circular(30)
            ),
            margin: EdgeInsets.only(left: 5),
            width: 50.0,
            child: Center(child: Text('M $modulo')),
        );
    }
    return Container(
      height: 30,
      margin: EdgeInsets.symmetric(vertical: 25.0),
      child: Container(
        height: 100,
        decoration: BoxDecoration(
          color: Colors.grey[200]
        ),
        child: ListView.builder(
                  controller: _scrollController,
                  dragStartBehavior: DragStartBehavior.down,
                  scrollDirection: Axis.horizontal,
                  itemCount: length,
                  itemBuilder: (BuildContext context, int index){
                    return _modulo(index + 1);
                  },
                ),
      ),
    );
  }

  Widget _botonReset() {
      return Center(
       child: Column(
         children: <Widget>[
           Expanded(
             child: Container(),
           ),
           FloatingActionButton(
             heroTag: null,
              elevation: 10,
              backgroundColor: Colors.red,
              child: Icon(Icons.restore, size: 40),
              onPressed: (){
                paso = '';
                setState(() {
                  
                });
              },
          ),
          SizedBox(height: 10,)
         ], 
       ),
    );
  }

  @override
  Widget build(BuildContext context) {
    double _altura = MediaQuery.of(context).size.height;
    double _ancho = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(_altura * 0.15),
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(bottomLeft: Radius.circular(30),bottomRight: Radius.circular(30)),
          ),
          child: AppBar(
            iconTheme: IconThemeData(
              color: Colors.black
            ),
            elevation: 10,
            title: Text('Crea tu nuevo paso', style: TextStyle(fontSize: 25, color: Colors.red),),
            backgroundColor: Colors.white,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.vertical(
              bottom: Radius.circular(30),
              ),
            ),
          ),
        ),
      ),
      endDrawer: _drawer(_altura, _ancho, context),
      body: Stack(
        children: <Widget>[
          _contentenedor(_altura, _ancho),
          Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
              SizedBox(width: 10,),
              _luces(_ancho, 1, 'primero', _ancho),
              SizedBox(height: 10, width: 10,),
              _luces(_ancho, 2, 'segundo',_ancho),
              SizedBox(height: 10, width: 10,),
              _luces(_ancho, 3, 'tercero',_ancho),
              SizedBox(height: 10, width: 10,),
              _luces(_ancho, 4, 'cuarto', _ancho),
              SizedBox(height: 10, width: 10,),
              _luces(_ancho, 5, 'quinto', _ancho),
              SizedBox(height: 10, width: 10,),
              _luces(_ancho, 6, 'sexto', _ancho),
              ],
            ),
          ),
          _listaRutina(),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Builder(
                builder: (context){
                  return  _boton(context);
                },
              ),
              _botonReset(),
            ],
          ),
        ],
      ),
    );
  }
}