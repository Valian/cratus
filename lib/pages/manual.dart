import 'dart:async';

import 'package:cratus/shared_prefs/user_prefs.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class ManualPage extends StatefulWidget {
  @override
  _ManualPage createState() => _ManualPage();
}

class _ManualPage extends State<ManualPage> {
  String iconoColor = 'assets/img/botones_de_colores.png';
  String iconoNumero = 'assets/img/boton_numeros_encendido.png';
  final prefs = PreferenciaUsuario();
  static const cratusChannel  = const MethodChannel('cratus');
  static const _stream        = const EventChannel('estados-cratus');
  String _mensajes = '';
  String _oldMensajes = 'old';
  String _msj = '';
  StreamSubscription subscription;

  Color _cuarto; 
  Color _primero;
  Color _quinto;
  Color _segundo;
  Color _sexto;
  Color _tercero;

  Timer _timer;

  @override
  void dispose() {
    subscription.cancel();
    _timer.cancel();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    // _iniciar();
    _familia();


      _timer = Timer.periodic(
        Duration(milliseconds: 10),
         (timer){
        //     _mensaje();
          subscription = _stream.receiveBroadcastStream().listen((mensaje){
          setState(() {
            _msj = mensaje;
          });
          _oldMensajes = _mensajes;
          _mensajes = _msj;

        });
             if(_mensajes != _oldMensajes && _mensajes != null && _mensajes.length>1){
              List<String> disp = _mensajes.split(";");
              String _ultimo = disp[disp.length - 1];
              String apagar = _ultimo.substring(0,_ultimo.length - 1);
              if(apagar == '01'){
                _primero = Colors.transparent;
                setState(() {});
              }
              if(apagar == '02'){
                _segundo = Colors.transparent;
                setState(() {});
              }
              if(apagar == '03'){
                _tercero = Colors.transparent;
                setState(() {});
              }
              if(apagar == '04'){ 
                _cuarto = Colors.transparent;
                setState(() {});
              }
              _primero = Colors.transparent;
              if(apagar == '05'){
                _quinto = Colors.transparent;
                setState(() {});
              }
              if(apagar == '06'){
                _sexto = Colors.transparent;
                setState(() {});
              }
             }
         }
      );

    // String string;
    // for(int i = 1; i<=6; i++){
    //   string = "0002;0$i;04;00000000000000FF0000";
    //   _string(string);
    // }
    //primero
    if(prefs.primero == 'red')
      _primero = Colors.red;
    if(prefs.primero == 'green')
      _primero = Colors.green;
    if(prefs.primero == 'yellow')
      _primero = Colors.yellow;
    if(prefs.primero == 'purple')
      _primero = Colors.purple;
    //segundo
    if(prefs.segundo == 'red')
      _segundo = Colors.red;
    if(prefs.segundo == 'green')
      _segundo = Colors.green;
    if(prefs.segundo == 'yellow')
      _segundo = Colors.yellow;
    if(prefs.segundo == 'purple')
      _segundo = Colors.purple;
    //tercero
    if(prefs.tercero == 'red')
      _tercero = Colors.red;
    if(prefs.tercero == 'green')
      _tercero = Colors.green;
    if(prefs.tercero == 'yellow')
      _tercero = Colors.yellow;
    if(prefs.tercero == 'purple')
      _tercero = Colors.purple;
    //cuarto
    if(prefs.cuarto == 'red')
      _cuarto = Colors.red;
    if(prefs.cuarto == 'green')
      _cuarto = Colors.green;
    if(prefs.cuarto == 'yellow')
      _cuarto = Colors.yellow;
    if(prefs.cuarto == 'purple')
      _cuarto = Colors.purple;
    //quinto
    if(prefs.quinto == 'red')
      _quinto = Colors.red;
    if(prefs.quinto == 'green')
      _quinto = Colors.green;
    if(prefs.quinto == 'yellow')
      _quinto = Colors.yellow;
    if(prefs.quinto == 'purple')
      _quinto = Colors.purple;
    //sexto
    if(prefs.sexto == 'red')
      _sexto = Colors.red;
    if(prefs.sexto == 'green')
      _sexto = Colors.green;
    if(prefs.sexto == 'yellow')
      _sexto = Colors.yellow;
    if(prefs.sexto == 'purple')
      _sexto = Colors.purple;

  }
    Future<void> _familia() async{
      try {
        String result = await cratusChannel.invokeMethod("familia");
        prefs.familia = result;
      } on PlatformException catch (e) {
        print(e);
      }
    }


    _mensaje() async{
      try {
        String result = await cratusChannel.invokeMethod("msj");
        if(result != null){
          List<String> disp = result.split(";");
          if(disp != null){
            if(disp.length >= 3){
              if(disp[disp.length-1].length > 1){
                if(_mensajes != _oldMensajes){
                 _oldMensajes = _mensajes;
                  _mensajes = disp[disp.length - 1];
                }
              }
              if(disp[disp.length-1].length == 1 && disp[disp.length-1].length != null){
              _mensajes = '';
            }
            } 
          }
        }
      } on PlatformException catch (e) {
        print(e);
      }
    }
    Future<void> _string(String string) async{
      try {
        await cratusChannel.invokeMethod("string", {
          'string' : string
        });
      } on PlatformException catch (e) {
        print(e);
      }
    }
    Widget _contentenedor(altura, anchura) {
    return Container(
      margin: EdgeInsets.all(anchura*0.05),
      height: altura * 0.8,
      decoration: BoxDecoration(
        color: Color.fromRGBO(224, 226, 228, 1),
        borderRadius: BorderRadius.circular(20),
      ),
      child: Container(
        margin: EdgeInsets.all(20),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(30)
        ),
      ),
    );
  }

  Widget _luces(ancho, contador, String fila, double alto) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
        _crearBotonPrincipal(iconoNumero, contador, alto, ancho),
        SizedBox(width: ancho *0.01,),
        _crearBoton(iconoColor, Colors.green, ancho, fila, alto),
        SizedBox(width: ancho *0.01,),
        _crearBoton(iconoColor, Colors.yellow, ancho, fila, alto),
        SizedBox(width: ancho *0.01,),
        _crearBoton(iconoColor, Colors.red, ancho, fila, alto),
        SizedBox(width: ancho *0.01,),
        _crearBoton(iconoColor, Colors.purple, ancho, fila, alto),
      ],
    );
  }

  Widget _crearBoton(String icono, Color color, double ancho,String fila, double alto) {
    Color fondo;
    String _color;
    String _fila;
    if(fila == 'primero'){
      if(_primero == Colors.transparent)
        fondo = color.withOpacity(0.3);
      if(_primero == color)
        fondo = color;
      if(_primero != color)
        fondo = color.withOpacity(0.3);
    }
       if(fila == 'segundo'){
      if(_segundo == Colors.transparent)
        fondo = color.withOpacity(0.3);
      if(_segundo == color)
        fondo = color;
      if(_segundo != color)
        fondo = color.withOpacity(0.3);
    }
       if(fila == 'tercero'){
      if(_tercero == Colors.transparent)
        fondo = color.withOpacity(0.3);
      if(_tercero == color)
        fondo = color;
      if(_tercero != color)
        fondo = color.withOpacity(0.3);
    }
       if(fila == 'cuarto'){
      if(_cuarto == Colors.transparent)
        fondo = color.withOpacity(0.3);
      if(_cuarto == color)
        fondo = color;
      if(_cuarto != color)
        fondo = color.withOpacity(0.3);
    }
       if(fila == 'quinto'){
      if(_quinto == Colors.transparent)
        fondo = color.withOpacity(0.3);
      if(_quinto == color)
        fondo = color;
      if(_quinto != color)
        fondo = color.withOpacity(0.3);
    }
       if(fila == 'sexto'){
      if(_sexto == Colors.transparent)
        fondo = color.withOpacity(0.5);
      if(_sexto == color)
        fondo = color;
      if(_sexto != color)
        fondo = color.withOpacity(0.5);
    }

    _setLinea(){
      if(fila == 'primero'){
        _fila = '01';
        _primero = color;
        if(color == Colors.red){
          _primero = Colors.red;
          prefs.primero = 'red';
          _color = 'FF0000';
        }
        if(color == Colors.green){
          _primero = Colors.green;
          prefs.primero = 'green';
          _color = '00FF00';
        }
        if(color == Colors.yellow){
          _primero = Colors.yellow;
          prefs.primero = 'yellow';
          _color = 'FFFF00';
        }
        if(color == Colors.purple){
          _primero = Colors.purple;
          prefs.primero = 'purple';
          _color = 'FF00FF';
        }
      }
      if(fila == 'segundo'){
        _fila = '02';
        _segundo = color;
        if(color == Colors.red){
          _segundo = Colors.red;
          prefs.segundo = 'red';
          _color = 'FF0000';
        }
        if(color == Colors.green){
          _segundo = Colors.green;
          prefs.segundo = 'green';
          _color = '00FF00';
        }
        if(color == Colors.yellow){
          _segundo = Colors.yellow;
          prefs.segundo = 'yellow';
          _color = 'FFFF00';
        }
        if(color == Colors.purple){
          _segundo = Colors.purple;
          prefs.segundo = 'purple';
          _color = 'FF00FF';
        }
      }
        if(fila == 'tercero'){
          _fila = '03';
         _tercero = color;
        if(color == Colors.red){
          _tercero = Colors.red;
          prefs.tercero = 'red';
          _color = 'FF0000';
        }
        if(color == Colors.green){
          _tercero = Colors.green;
          prefs.tercero = 'green';
          _color = '00FF00';
        }
        if(color == Colors.yellow){
          _tercero = Colors.yellow;
          prefs.tercero = 'yellow';
          _color = 'FFFF00';
        }
        if(color == Colors.purple){
          _tercero = Colors.purple;
          prefs.tercero = 'purple';
          _color = 'FF00FF';
        }
      }
        if(fila == 'cuarto'){
          _fila = '04';
        _cuarto = color;
        if(color == Colors.red){
          _cuarto = Colors.red;
          prefs.cuarto = 'red';
          _color = 'FF0000';
        }
        if(color == Colors.green){
          _cuarto = Colors.green;
          prefs.cuarto = 'green';
          _color = '00FF00';
        }
        if(color == Colors.yellow){
          _cuarto = Colors.yellow;
          prefs.cuarto = 'yellow';
          _color = 'FFFF00';
        }
        if(color == Colors.purple){
          _cuarto = Colors.purple;
          prefs.cuarto = 'purple';
          _color = 'FF00FF';
        }
      }
        if(fila == 'quinto'){
          _fila = '05';
        _quinto = color;
        if(color == Colors.red){
          _quinto = Colors.red;
          prefs.quinto = 'red';
          _color = 'FF0000';
        }
        if(color == Colors.green){
          _quinto = Colors.green;
          prefs.quinto = 'green';
          _color = '00FF00';
        }
        if(color == Colors.yellow){
          _quinto = Colors.yellow;
          prefs.quinto = 'yellow';
          _color = 'FFFF00';
        }
        if(color == Colors.purple){
          _quinto = Colors.purple;
          prefs.quinto = 'purple';
          _color = 'FF00FF';
        }
      }
        if(fila == 'sexto'){
          _fila = '06';
         _sexto = color;
        if(color == Colors.red){
          _sexto = Colors.red;
          prefs.sexto = 'red';
          _color = 'FF0000';
        }
        if(color == Colors.green){
          _sexto = Colors.green;
          prefs.sexto = 'green';
          _color = '00FF00';
        }
        if(color == Colors.yellow){
          _sexto = Colors.yellow;
          prefs.sexto = 'yellow';
          _color = 'FFFF00';
        }
        if(color == Colors.purple){
          _sexto = Colors.purple;
          prefs.sexto = 'purple';
          _color = 'FF00FF';
        }
      }
    }
    return Container(
      margin: EdgeInsets.all(ancho*0.01),
      width: ancho * 0.125,
      height: alto * 0.125,
      child: FloatingActionButton(
          backgroundColor: fondo,
          onPressed: (){
            setState(() {
             _setLinea(); 
              String string = "${prefs.familia};$_fila;03;0000ffff${_color}020102";
             _string(string);
            });
          },
          elevation: 10,
          heroTag: null,
          child: Image(
            height: alto * 0.2,
            image: AssetImage(icono),
            fit: BoxFit.cover,
          ),
      ),
    );
  }

   Widget _crearBotonPrincipal(String icono, int contador, double alto, double ancho) {
    return Container(
      margin: EdgeInsets.all(ancho*0.01),
      width: ancho * 0.125,
      height: alto * 0.125,
      child: Stack(
          children: <Widget>[
          Image(
            height: alto * 0.2,
          image: AssetImage(icono),
          fit: BoxFit.cover,
        ),
        Center(child: Text(contador.toString(), style: TextStyle(fontSize: 40, color: Colors.black),))
          ],
        )
    );
  }

  @override
  Widget build(BuildContext context) {
    double _altura = MediaQuery.of(context).size.height;
    double _ancho = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(_altura * 0.15),
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(bottomLeft: Radius.circular(30),bottomRight: Radius.circular(30)),
            image: DecorationImage(
              image: AssetImage('assets/img/barra_superior_manual.png'),
              fit: BoxFit.cover,
            ),
          ),
          child: AppBar(
            elevation: 10,
            title: Text(_msj, style: TextStyle(fontSize: 25),),
            backgroundColor: Colors.transparent,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.vertical(
              bottom: Radius.circular(30),
              ),
            ),
          ),
        ),
      ),
      body: Stack(
        children: <Widget>[
          _contentenedor(_altura, _ancho),
          Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                SizedBox(width: 10,),
                _luces(_ancho, 1, 'primero', _ancho),
                SizedBox(height: 10, width: 10,),
                _luces(_ancho, 2, 'segundo',_ancho),
                SizedBox(height: 10, width: 10,),
                _luces(_ancho, 3, 'tercero',_ancho),
                SizedBox(height: 10, width: 10,),
                _luces(_ancho, 4, 'cuarto', _ancho),
                SizedBox(height: 10, width: 10,),
                _luces(_ancho, 5, 'quinto', _ancho),
                SizedBox(height: 10, width: 10,),
                _luces(_ancho, 6, 'sexto', _ancho),
              ],
            ),
          ),
        ],
      ),
    );
  }
}