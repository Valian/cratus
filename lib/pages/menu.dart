import 'package:flutter/material.dart';

class MenuPage extends StatefulWidget {
  @override
  _MenuPageState createState() => _MenuPageState();
}

class _MenuPageState extends State<MenuPage> {
  String imagenAleatorio = 'assets/img/btn_aleatorio.png';
  String imagenCrearRutina = 'assets/img/btn_crear_rutina.png';
  String imagenEstatico = 'assets/img/btn_estatico.png';
  String imagenManual = 'assets/img/btn_manual.png';

  Widget _crearBoton(String imagen, String ruta, double altura, double ancho) {
    return GestureDetector(
      onTap: (){
        Navigator.pushNamed(context, ruta);
      },
      child: ClipRRect(
        borderRadius: BorderRadius.circular(40),
        child: Container(
          height: altura * 0.3,
          margin: EdgeInsets.all(10),
          alignment: Alignment.center,
          decoration: BoxDecoration(
            image: DecorationImage(
              image:AssetImage(imagen),
              fit: BoxFit.cover,
            ),
            borderRadius: BorderRadius.circular(40)
          )
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    double _altura = MediaQuery.of(context).size.height;
    double _ancho = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(_altura * 0.15),
        child: Container(
          child: AppBar(
            leading: Icon(Icons.arrow_left, color: Colors.black),
            elevation: 10,
            title: Text('PROGRAMAS', style: TextStyle(fontSize: 25, color: Colors.red),),
            backgroundColor: Colors.white,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.vertical(
              bottom: Radius.circular(30),
              ),
            ),
          ),
        ),
      ),
      body: Center(
        child: Table(
          children: [
            TableRow(
              children: [
                  _crearBoton(imagenManual, 'manual', _altura, _ancho),
                  _crearBoton(imagenAleatorio, 'aleatoria', _altura, _ancho),
              ]
            ),
            TableRow(
              children: [
                  _crearBoton(imagenCrearRutina, 'rutina', _altura, _ancho),
                  _crearBoton(imagenEstatico, 'estatica', _altura, _ancho),
              ]
            ),
          ]
        ),
      )
    );
  }
}