import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PreferenciaUsuario{
      //aleatorioGreen
  get aleatorioGreen{
    return _prefs.getBool('aleatorioGreen') ?? false;
  }

  //aleatoriopurple
  get aleatoriopurple{
    return _prefs.getBool('aleatoriopurple') ?? false;
  }

  //aleatorioRed
  get aleatorioRed{
    return _prefs.getBool('aleatorioRed') ?? false;
  }

      //aleatorioYellow
  get aleatorioYellow{
    return _prefs.getBool('aleatorioYellow') ?? false;
  }

    //cuarto
  get cuarto{
    return _prefs.getString('cuarto') ?? Colors.transparent;
  }

  //mac
   get mac{
    return _prefs.getString('mac') ?? '';
  }

  get pasos{
    List<String> lista = [];
    return _prefs.getStringList('pasos') ?? lista;
  }

  //primero
  get primero{
    return _prefs.get('primero') ?? Colors.transparent;
  }

    //quinto
  get quinto{
    return _prefs.getString('quinto') ?? Colors.transparent;
  }

    //segundo
  get segundo{
    return _prefs.getString('segundo') ?? Colors.transparent;
  }

    //sexto
  get sexto{
    return _prefs.getString('sexto') ?? Colors.transparent;
  }

    //tercero
  get tercero{
    return _prefs.getString('tercero') ?? Colors.transparent;
  }

    //tercero
  get familia{
    return _prefs.getString('familia') ?? '';
  }

  static final PreferenciaUsuario _instancia = new PreferenciaUsuario._internal();

  SharedPreferences _prefs;

  factory PreferenciaUsuario(){
    return _instancia;
  }

  PreferenciaUsuario._internal();

    initPrefs() async {
    _prefs = await SharedPreferences.getInstance();
  }

  set primero(String value){
    _prefs.setString('primero', value);
  }

  set segundo(String value){
    _prefs.setString('segundo', value);
  }

  set tercero(String value){
    _prefs.setString('tercero', value);
  }

  set cuarto(String value){
    _prefs.setString('cuarto', value);
  }

  set quinto(String value){
    _prefs.setString('quinto', value);
  }

  set sexto(String value){
    _prefs.setString('sexto', value);
  }

  set aleatorioRed(bool value){
    _prefs.setBool('aleatorioRed', value);
  }

  set aleatoriopurple(bool value){
    _prefs.setBool('aleatoriopurple', value);
  }

    set familia(String familia){
    _prefs.setString('familia', familia);
  }

  set aleatorioGreen(bool value){
    _prefs.setBool('aleatorioGreen', value);
  }

  set aleatorioYellow(bool value){
    _prefs.setBool('aleatorioYellow', value);
  }

  set pasos(List<String> value){
     _prefs.setStringList('pasos', value);
  }

    set mac(String value){
    _prefs.setString('mac', value);
  }
}