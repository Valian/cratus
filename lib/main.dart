import 'package:cratus/pages/aleatoria.dart';
import 'package:cratus/pages/conectar.dart';
import 'package:cratus/pages/estatica.dart';
import 'package:cratus/pages/home.dart';
import 'package:cratus/pages/inicio_page.dart';
import 'package:cratus/pages/juegos.dart';
import 'package:cratus/pages/manual.dart';
import 'package:cratus/pages/menu.dart';
import 'package:cratus/pages/perfil.dart';
import 'package:cratus/pages/resultado.dart';
import 'package:cratus/pages/run_rutina.dart';
import 'package:cratus/pages/rutina.dart';
import 'package:cratus/shared_prefs/user_prefs.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
 
void main()async{
  final prefs = new PreferenciaUsuario();
  await prefs.initPrefs();
  runApp(MyApp());
}
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light.copyWith(
      statusBarColor: Colors.transparent
    ));
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitDown,
      DeviceOrientation.portraitUp,
  ]);

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Cratus',
      initialRoute: 'home',
      theme: ThemeData(
        fontFamily: 'Helvetica',
      ),
      routes: {
        'home'      : (BuildContext context) => HomePage(),
        'inicio'    : (BuildContext context) => InicioPage(),
        'menu'      : (BuildContext context) => MenuPage(),
        'conectar'  : (BuildContext context) => Conectar(),
        'juegos'    : (BuildContext context) => JuegosPage(),
        'perfil'    : (BuildContext context) => PerfilPage(),
        'aleatoria' : (BuildContext context) => AleatoriaPage(),
        'manual'    : (BuildContext context) => ManualPage(),
        'estatica'  : (BuildContext context) => EstaticaPage(),
        'rutina'    : (BuildContext context) => RutinaPage(),
        'resultado' : (BuildContext context) => ResultadoPage(),
        'run'       : (BuildContext context) => RunRutina()
      },
    );
  }
}