package com.example.cratos;

import android.os.Bundle;
import io.flutter.app.FlutterActivity;
import io.flutter.plugins.GeneratedPluginRegistrant;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.plugin.common.EventChannel;
import io.flutter.plugin.common.EventChannel.EventSink;
import io.flutter.plugin.common.EventChannel.StreamHandler;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleExpandableListAdapter;



import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import static java.lang.Integer.parseInt;
import static java.lang.Integer.toHexString;


/* import io.reactivex.Observable; */

public class MainActivity extends FlutterActivity {

    private static final String CHANNEL = "cratus";
    public static final String STREAM = "estados-cratus";
    ArrayList<String> dispositivos = new ArrayList<>();
    private List<BluetoothDevice> listBluetoothDevice;
    private BluetoothLeScanner bluetoothLeScanner;
    public BluetoothAdapter bluetoothAdapter;
    private ScanSettings settings;
    

    private int REQUEST_ENABLE_BT = 1;
    // private static final UUID MY_UUID =
    // UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    private static final long SCAN_PERIOD = 800;
    private Handler handler;

    public BluetoothLeService services;
    private boolean mConnected = false;
    String address;
    String modulos, family, msg, msjRetorno;
    private char firsRead = 0;

    private final String LIST_NAME = "NAME";
    private final String LIST_UUID = "UUID";
    private ArrayList<ArrayList<BluetoothGattCharacteristic>> mGattCharacteristics = new ArrayList<>();
    private BluetoothGattCharacteristic bluetoothGattCharacteristicHM_10;
    private BluetoothGattCharacteristic mNotifyCharacteristic;
    private ExpandableListView mGattServicesList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GeneratedPluginRegistrant.registerWith(this);
        
        new MethodChannel(getFlutterView(), CHANNEL).setMethodCallHandler(new MethodCallHandler() {
            @Override
            public void onMethodCall(MethodCall call, Result result) {
                if (call.method.equals("buscar")) {
                    modulos = "01";
                    initAdapter();
                    initialize();
                    scanLeDevice();
                    
                    for (String dis : dispositivos) {
                        Log.i("DISPOSIT ", "------------- " + dis);
                    }
                    if (dispositivos != null) {
                        result.success(dispositivos);
                    } else {
                        result.error("Desconectado", "Bluetooth desconectado.", null);
                    }
                }
                if (call.method.equals("conectar")) {
                    final String mac = call.argument("mac");
                    initConnetion(mac);
                    result.success(dispositivos);
                }
                if (call.method.equals("string")) {
                    final String string = call.argument("string");
                    services.sendMessage((string + "\n").getBytes());
                    result.success(dispositivos);
                }
                if (call.method.equals("familia")) {
                    result.success(family);
                }
                if (call.method.equals("desc")) {
                    services.close();
                    result.success(msjRetorno);
                }
                if (call.method.equals("msj")) {
          /*           Log.i("MENSAJE", msjRetorno); */
                    result.success(msjRetorno);
                } else {
                    result.notImplemented();
                }
            }
        }); //fin method channel

        new EventChannel(getFlutterView(), STREAM).setStreamHandler(
            new EventChannel.StreamHandler() {
                @Override
                public void onListen(Object args, final EventChannel.EventSink events) {
                    events.success(msjRetorno); 
                }

                @Override
                public void onCancel(Object args) {
                    
                }
            }
        );

        
        
    }

    /**
     * initialize(), inicializa el dispositivo bluetooth y los permisos necesarios.
     */

    public boolean initAdapter() {

        final BluetoothManager bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        if (bluetoothManager != null) {
            bluetoothAdapter = bluetoothManager.getAdapter();
        }
        if (bluetoothAdapter == null || !bluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            int REQUEST_ENABLE_BT = 1;
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            return false;
        }

        return true;
    }

    public void initialize() {

        int MY_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION = 1;

        ActivityCompat.requestPermissions(this, new String[] { Manifest.permission.ACCESS_COARSE_LOCATION },
                MY_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION);

        handler = new Handler();
        bluetoothLeScanner = bluetoothAdapter.getBluetoothLeScanner();
        listBluetoothDevice = new ArrayList<>();

    }

    /**          Log.i("MENSAJE", msjRetorno);
     * scanLeDevice(), inicia el periodo de escaneo.
     */

    private void scanLeDevice() {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                bluetoothLeScanner.stopScan(leScanCallback);
            }
        }, SCAN_PERIOD);

        bluetoothLeScanner.startScan(leScanCallback);

    }

    /**
     * leScanCallback, entrega la lista de dispositivos
     */

    private ScanCallback leScanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            super.onScanResult(callbackType, result);
            Log.i("callbackType", String.valueOf(callbackType));
            Log.i("result", result.toString());
            addBluetoothDevice(result.getDevice());
        }

        @Override
        public void onBatchScanResults(List<ScanResult> results) {
            super.onBatchScanResults(results);
            for (ScanResult sr : results) {
                Log.i("ScanResult - Results", sr.toString());
                addBluetoothDevice(sr.getDevice());
            }
        }

        @Override
        public void onScanFailed(int errorCode) {
            super.onScanFailed(errorCode);
            Log.e("Scan Failed", "Error Code: " + errorCode);
        }

        private void addBluetoothDevice(BluetoothDevice device) {
            if (!listBluetoothDevice.contains(device)) {
                listBluetoothDevice.add(device);
                dispositivos.add(device.getName() + " MAC-DISP == " + device.getAddress());
                Log.i("LISTA", device.getName() + "" + device.getAddress());
            }
        }
    };

    /**
     * onItemClickListener, toma la direccion del dispositivo seleccionado en la
     * lista llama al metodo connect desde la clase BluetoothLeService
     */
    public void initConnetion(String direccion) {
        address = direccion;
        Intent gattServiceIntent = new Intent(getApplicationContext(), BluetoothLeService.class);
        bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);
        if (services != null) {
            boolean result = services.connect(address, bluetoothAdapter);
            Log.d("CONEXION", "Connect request result=" + result);
        }
    }

    public final ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            services = ((BluetoothLeService.LocalBinder) service).getService();
            if (!services.initAdapter()) {
                Log.e("CON", "Unable to initialize Bluetooth");
                finish();
            }
            // Automatically connects to the device upon successful start-up initialization.
            services.connect(address, bluetoothAdapter);

        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            Log.i("LLEGA AQUI", "disconect");
            services = null;
        }
    };

    protected void onResume() {
        super.onResume();
        initAdapter();
        registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
        if (services != null) {
            boolean result = services.connect(address, bluetoothAdapter);
            Log.d("conexion", "Connect request result=" + result);
        }
    }

    private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            Log.i("ACTION BROAD", action);
            if (BluetoothLeService.ACTION_GATT_CONNECTED.equals(action)) {
                mConnected = true;
                Log.i("ESTADO", "CONECTADO");
                invalidateOptionsMenu();
                try {
                    Thread.sleep(800);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    Log.i("Log delay", e.toString());
                }
                services.sendMessage(("0000;00;0A;00000000000000000000\n").getBytes());
            } else if (BluetoothLeService.ACTION_GATT_DISCONNECTED.equals(action)) {
                mConnected = false;
                Log.i("ESTADO", "DESCONECTADO");
                invalidateOptionsMenu();
            } else if (BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {
                // Show all the supported services and characteristics on the user interface.
                displayGattServices(services.getSupportedGattServices());
            } else if (BluetoothLeService.ACTION_DATA_AVAILABLE.equals(action)) {   
                 msjRetorno= displayData(intent.getStringExtra(BluetoothLeService.EXTRA_DATA));
                 ControlAdapter control = new ControlAdapter();
                 control.setRespuesta(msjRetorno);
                
                // Log.i("mensaje-------", msjRetorno);
                
            }
        }
    };

    private String displayData(String data) {
        String[] array;
        String msg = "";
        if (data != null) {
            msg = msg + data.substring(0, data.indexOf("\n"));
            if (msg.contains("$")) {
                if (msg.contains(";")) {
                    array = msg.split(";");
                    if (array[2].equals("0A")) {
                        family = array[3].substring(0, array[3].indexOf("$"));

                        services.sendMessage((family + ";01;08;00000000000000000000\n").getBytes());
                    }
                    if (array[2].equals("00")) {
                        Log.i("00", "familia 00");
                    }
                }
            }
            return msg;
        }
        // String[] array;
        // if(firsRead == 0) {
        // data = null;
        // firsRead = 1;
        // msg = "";
        // }
        // if (data != null) {
        // msg = msg + data.substring(0,data.indexOf("\n"));
        // if(msg.contains("$")) {
        // if(msg.contains(";")){
        // array = msg.split(";");
        // if(array[2].equals("0A")){
        // family = array[3].substring(0,array[3].indexOf("$"));

        // services.sendMessage((family + ";01;08;00000000000000000000\n").getBytes());
        // }
        // if(array[2].equals("00")){
        // Log.i("00","familia 00");
        // }
        // }
        // msg = "";
        // }
        // }
        else {
            return msg;
        }
    }

    public void displayGattServices(List<BluetoothGattService> gattServices) {

        UUID UUID_HM_10 = UUID.fromString(SampleGattAttributes.HM_10);

        if (gattServices == null)
            return;
        String uuid = null;
        String unknownServiceString = getResources().getString(R.string.unknown_service);
        String unknownCharaString = getResources().getString(R.string.unknown_characteristic);
        ArrayList<HashMap<String, String>> gattServiceData = new ArrayList<>();
        ArrayList<ArrayList<HashMap<String, String>>> gattCharacteristicData = new ArrayList<ArrayList<HashMap<String, String>>>();
        mGattCharacteristics = new ArrayList<>();

        // Loops through available GATT Services.
        for (BluetoothGattService gattService : gattServices) {
            HashMap<String, String> currentServiceData = new HashMap<String, String>();
            uuid = gattService.getUuid().toString();
            currentServiceData.put(LIST_NAME, SampleGattAttributes.lookup(uuid, unknownServiceString));
            currentServiceData.put(LIST_UUID, uuid);
            gattServiceData.add(currentServiceData);
            ArrayList<HashMap<String, String>> gattCharacteristicGroupData = new ArrayList<HashMap<String, String>>();
            List<BluetoothGattCharacteristic> gattCharacteristics = gattService.getCharacteristics();
            ArrayList<BluetoothGattCharacteristic> charas = new ArrayList<BluetoothGattCharacteristic>();

            // Loops through available Characteristics.
            for (BluetoothGattCharacteristic gattCharacteristic : gattCharacteristics) {
                charas.add(gattCharacteristic);
                HashMap<String, String> currentCharaData = new HashMap<String, String>();
                uuid = gattCharacteristic.getUuid().toString();
                currentCharaData.put(LIST_NAME, SampleGattAttributes.lookup(uuid, unknownCharaString));
                currentCharaData.put(LIST_UUID, uuid);
                gattCharacteristicGroupData.add(currentCharaData);

                // Check if it is "HM_10"
                if (uuid.equals(SampleGattAttributes.HM_10)) {
                    bluetoothGattCharacteristicHM_10 = gattService.getCharacteristic(UUID_HM_10);
                    services.readCharacteristic(bluetoothGattCharacteristicHM_10);
                    mNotifyCharacteristic = bluetoothGattCharacteristicHM_10;
                    services.setCharacteristicNotification(bluetoothGattCharacteristicHM_10, true);

                }
            }
            mGattCharacteristics.add(charas);
            gattCharacteristicData.add(gattCharacteristicGroupData);
        }
    }

    public static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(BluetoothLeService.ACTION_DATA_AVAILABLE);
        return intentFilter;
    }

    public interface IEscucha{
        public void mensaj(String data);
    }
    public class A implements IEscucha{
        @Override
        public void mensaj(String data) {
            System.out.println("Desde A hemos recibido :"+ data);
            

        }
    }
    public class B{
        private IEscucha receptor;
        public B(IEscucha receptor)
        {
            this.receptor = receptor;
        }
        public String data;
        public void cambiandoEstado()
        {
            data = msjRetorno;
            receptor.mensaj(data);
        }
    }
    
}